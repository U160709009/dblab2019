create view usa_customers as 

select CustomerID , CustomerName ,ContactName 
from customers
where country = "USA";

select * from usa_customers;

select *
from usa_customers join orders on usa_customers.CustomerID = Orders.CustomerID;

select avg(price) from Products;

create or replace view products_below_avg_price as

select ProductID , ProductName , Price
from Products
where price < 30;

select *
 from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID 
 where OrderID in
(
	select OrderID 
    from OrderDetails join product_below_avg_price on OrderDetails.ProductID = product_below_avg_price.ProductID);
    


drop view usa_customers;

